const fs = require('fs')
const path = require('path')

const packageContent = {
  name: 'uni-pay',
  version: '1.0.10',
  description: 'unipay for uniCloud',
  main: 'index.js',
  homepage: 'https://uniapp.dcloud.io/uniCloud/unipay',
  repository: {
    type: 'git',
    url: 'git+https://gitee.com/dcloud/uniPay.git'
  },
  scripts: {
  },
  keywords: [],
  author: '',
  license: 'Apache-2.0',
  devDependencies: {}
}
const { version } = require('../package.json')
packageContent.version = version

fs.writeFileSync(path.resolve(__dirname, '../dist/package.json'), JSON.stringify(packageContent, null, 2))
fs.copyFileSync(path.resolve(__dirname, '../LICENSE'), path.resolve(__dirname, '../dist/LICENSE.md'))
