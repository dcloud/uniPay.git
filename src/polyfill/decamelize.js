// 仅被alipay-sdk使用，此依赖过大，这里进行简化
import {
  camel2snake
} from '../shared/utils'

export default camel2snake
