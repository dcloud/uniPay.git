import crypto from 'crypto'

function sha256 (str, key, encoding = 'utf8') {
  return crypto
    .createHmac('sha256', key)
    .update(str, encoding)
    .digest('hex')
}
// 获取支付签名
function getPaySig (uri, postBody, appKey) {
  const needSignMsg = uri + '&' + postBody
  return sha256(needSignMsg, appKey)
}
// 获取用户态签名
function getSignature (postBody, appKey) {
  return sha256(postBody, appKey)
}

export default {
  sha256,
  getPaySig,
  getSignature
}
